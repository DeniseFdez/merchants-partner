$(document).ready(function(){
    $(".menu").click(function(){
        
        App.blockUI();
        
        var idParent=$(this).attr("data-parent");
        $(".page-content").load($(this).attr("data-href"),[],function(){
            App.unblockUI();
        });
        $(".open").attr("class","");  
        $("#idParent").attr("class","open");  

    }); 
    
    $("#menuDashboar_ a").trigger("click");
    $("#menuDashboar_").attr("class","open");  
});