<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDriver
 *
 * @ORM\Table(name="order_driver", indexes={@ORM\Index(name="fk_order_driver_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_order_driver_drivers1_idx", columns={"driver_id"})})
 * @ORM\Entity
 */
class OrderDriver
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_driver_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderDriverId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="driver_id", type="integer", nullable=false)
     */
    private $driverId;


}

