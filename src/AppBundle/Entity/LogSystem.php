<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogSystem
 *
 * @ORM\Table(name="log_system")
 * @ORM\Entity
 */
class LogSystem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="user", type="string", length=200, nullable=true)
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="roll", type="integer", nullable=true)
     */
    private $roll;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=200, nullable=true)
     */
    private $action;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", length=200, nullable=true)
     */
    private $class;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=200, nullable=true)
     */
    private $method;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="user_full_name", type="string", length=200, nullable=true)
     */
    private $userFullName;

    /**
     * @var integer
     *
     * @ORM\Column(name="application", type="integer", nullable=false)
     */
    private $application;


}

