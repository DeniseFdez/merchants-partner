<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DriverVerification
 *
 * @ORM\Table(name="driver_verification")
 * @ORM\Entity
 */
class DriverVerification
{
    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=100, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="secret_key", type="string", length=100, nullable=true)
     */
    private $secretKey;

    /**
     * @var string
     *
     * @ORM\Column(name="public_key", type="string", length=100, nullable=true)
     */
    private $publicKey;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tos_acceptance_date", type="datetime", nullable=true)
     */
    private $tosAcceptanceDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="tos_acceptance_ip", type="string", length=100, nullable=true)
     */
    private $tosAcceptanceIp;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=100, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="ssn_last_4", type="string", length=10, nullable=true)
     */
    private $ssnLast4;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=3, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="line1", type="string", length=250, nullable=true)
     */
    private $line1;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=7, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_day", type="string", length=2, nullable=true)
     */
    private $dobDay;

    /**
     * @var string
     *
     * @ORM\Column(name="dob_month", type="string", length=2, nullable=true)
     */
    private $dobMonth;

    /**
     * @var integer
     *
     * @ORM\Column(name="dob_year", type="integer", nullable=true)
     */
    private $dobYear;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_id_number", type="string", length=250, nullable=true)
     */
    private $personalIdNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="file_upload_id", type="string", length=100, nullable=true)
     */
    private $fileUploadId;

    /**
     * @var string
     *
     * @ORM\Column(name="routing_number", type="string", length=255, nullable=true)
     */
    private $routingNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=255, nullable=true)
     */
    private $accountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_id", type="string", length=255, nullable=true)
     */
    private $bankId;

    /**
     * @var integer
     *
     * @ORM\Column(name="document_file", type="integer", nullable=true)
     */
    private $documentFile = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="document_type", type="string", length=50, nullable=true)
     */
    private $documentType;

    /**
     * @var integer
     *
     * @ORM\Column(name="verified", type="integer", nullable=true)
     */
    private $verified;

    /**
     * @var \Drivers
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Drivers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id", referencedColumnName="id")
     * })
     */
    private $id;


}

