<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantsLocations
 *
 * @ORM\Table(name="merchants_locations", indexes={@ORM\Index(name="fk_merchants_locations_merchants1_idx", columns={"merchants_id"})})
 * @ORM\Entity
 */
class MerchantsLocations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="street_address", type="string", length=150, nullable=true)
     */
    private $streetAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=45, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="locality", type="string", length=45, nullable=true)
     */
    private $locality;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=45, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=10, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=150, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=45, nullable=true)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="long", type="string", length=45, nullable=true)
     */
    private $long;

    /**
     * @var string
     *
     * @ORM\Column(name="locu_id", type="string", length=20, nullable=true)
     */
    private $locuId;

    /**
     * @var \Merchants
     *
     * @ORM\ManyToOne(targetEntity="Merchants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="merchants_id", referencedColumnName="id")
     * })
     */
    private $merchants;


}

