<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payments
 *
 * @ORM\Table(name="payments", uniqueConstraints={@ORM\UniqueConstraint(name="id_2", columns={"id"})}, indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="id_3", columns={"id"}), @ORM\Index(name="driver_id", columns={"driver_id"})})
 * @ORM\Entity
 */
class Payments
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_peyment", type="datetime", nullable=false)
     */
    private $datePeyment;

    /**
     * @var float
     *
     * @ORM\Column(name="real_payment", type="float", precision=10, scale=0, nullable=false)
     */
    private $realPayment;

    /**
     * @var float
     *
     * @ORM\Column(name="stimate_payment", type="float", precision=10, scale=0, nullable=false)
     */
    private $stimatePayment;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="driver_id", type="integer", nullable=false)
     */
    private $driverId;

    /**
     * @var string
     *
     * @ORM\Column(name="transfer_number", type="string", length=250, nullable=true)
     */
    private $transferNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="list_id_orders", type="text", length=65535, nullable=true)
     */
    private $listIdOrders;


}

