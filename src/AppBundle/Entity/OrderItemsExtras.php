<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemsExtras
 *
 * @ORM\Table(name="order_items_extras", indexes={@ORM\Index(name="fk_order_items_extras_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_order_items_extras_merchants1_idx", columns={"merchant_id"}), @ORM\Index(name="fk_order_items_extras_merchant_products_extras_item1_idx", columns={"extra_id"}), @ORM\Index(name="fk_order_items_extras_merchant_products1_idx", columns={"product_id"}), @ORM\Index(name="fk_order_items_extras_merchant_products_extras_groups1_idx", columns={"group_id"})})
 * @ORM\Entity
 */
class OrderItemsExtras
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_item_extra_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderItemExtraId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_id", type="integer", nullable=false)
     */
    private $merchantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="extra_id", type="integer", nullable=false)
     */
    private $extraId;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=9, scale=2, nullable=true)
     */
    private $price;


}

