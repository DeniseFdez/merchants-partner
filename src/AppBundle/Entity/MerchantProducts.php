<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Merchants;
use AppBundle\Entity\MerchantsGroups;

/**
 * MerchantProducts
 *
 * @ORM\Table(name="merchant_products", indexes={@ORM\Index(name="fk_merchant_products_merchants1_idx", columns={"merchant_id"}), @ORM\Index(name="fk_merchant_products_merchants_groups1_idx", columns={"group_id"}), @ORM\Index(name="group_id", columns={"group_id"})})
 * @ORM\Entity
 */
class MerchantProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="restricted", type="integer", nullable=false)
     */
    private $restricted = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="all_day", type="integer", nullable=false)
     */
    private $allDay = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="time", nullable=true)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="time", nullable=true)
     */
    private $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $group;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_id", type="integer", nullable=false)
     */
    private $merchant;

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getPrice() {
        return $this->price;
    }

    function getRestricted() {
        return $this->restricted;
    }

    function getActive() {
        return $this->active;
    }

    function getAllDay() {
        return $this->allDay;
    }

    function getStartTime() {
        return $this->startTime;
    }

    function getEndTime() {
        return $this->endTime;
    }

    function getGroup() {
        return $this->group;
    }

    function getMerchant() {
        return $this->merchant;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setRestricted($restricted) {
        $this->restricted = $restricted;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setAllDay($allDay) {
        $this->allDay = $allDay;
    }

    function setStartTime( $startTime) {
        $this->startTime = $startTime;
    }

    function setEndTime( $endTime) {
        $this->endTime = $endTime;
    }

    function setGroup($group) {
        $this->group = $group;
    }

    function setMerchant($merchant) {
        $this->merchant = $merchant;
    }

}

