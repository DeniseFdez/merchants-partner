<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersResetcode
 *
 * @ORM\Table(name="customers_resetcode", indexes={@ORM\Index(name="fk_resetcode_customers1_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class CustomersResetcode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="member_email", type="string", length=30, nullable=false)
     */
    private $memberEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="reset_code", type="string", length=20, nullable=false)
     */
    private $resetCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_hour", type="datetime", nullable=false)
     */
    private $dateHour = 'CURRENT_TIMESTAMP';

    /**
     * @var \Customers
     *
     * @ORM\ManyToOne(targetEntity="Customers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;


}

