<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Voucher
 *
 * @ORM\Table(name="voucher", uniqueConstraints={@ORM\UniqueConstraint(name="promocode", columns={"promocode"})})
 * @ORM\Entity
 */
class Voucher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="promocode", type="string", length=30, nullable=false)
     */
    private $promocode;

    /**
     * @var integer
     *
     * @ORM\Column(name="deduction", type="integer", nullable=false)
     */
    private $deduction;

    /**
     * @var string
     *
     * @ORM\Column(name="expiration_date", type="string", length=30, nullable=false)
     */
    private $expirationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=30, nullable=false)
     */
    private $description;


}

