<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsedVoucher
 *
 * @ORM\Table(name="used_voucher", indexes={@ORM\Index(name="fk_used_voucher_customers1_idx", columns={"customer_id"}), @ORM\Index(name="fk_used_voucher_voucher1_idx", columns={"voucher_id"}), @ORM\Index(name="fk_used_voucher_orders1_idx", columns={"order_id"})})
 * @ORM\Entity
 */
class UsedVoucher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="voucher_id", type="integer", nullable=false)
     */
    private $voucherId;


}

