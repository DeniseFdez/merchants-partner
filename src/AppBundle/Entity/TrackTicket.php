<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrackTicket
 *
 * @ORM\Table(name="track_ticket", indexes={@ORM\Index(name="ticket_id", columns={"ticket_id"})})
 * @ORM\Entity
 */
class TrackTicket
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_change", type="datetime", nullable=false)
     */
    private $dateChange = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="state_ticket", type="integer", nullable=false)
     */
    private $stateTicket = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=false)
     */
    private $comment;

    /**
     * @var integer
     *
     * @ORM\Column(name="ticket_id", type="integer", nullable=false)
     */
    private $ticketId;

    /**
     * @var string
     *
     * @ORM\Column(name="answer_email", type="text", length=65535, nullable=true)
     */
    private $answerEmail;


}

