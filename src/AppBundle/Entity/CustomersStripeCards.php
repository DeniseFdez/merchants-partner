<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersStripeCards
 *
 * @ORM\Table(name="customers_stripe_cards", indexes={@ORM\Index(name="fk_customers_stripe_cards_customers1_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class CustomersStripeCards
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token_id", type="string", length=125, nullable=false)
     */
    private $tokenId;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_id", type="string", length=128, nullable=false)
     */
    private $stripeId;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=30, nullable=false)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="exp_month", type="string", length=4, nullable=false)
     */
    private $expMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="exp_year", type="string", length=4, nullable=false)
     */
    private $expYear;

    /**
     * @var string
     *
     * @ORM\Column(name="last4", type="string", length=4, nullable=false)
     */
    private $last4;

    /**
     * @var \Customers
     *
     * @ORM\ManyToOne(targetEntity="Customers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;


}

