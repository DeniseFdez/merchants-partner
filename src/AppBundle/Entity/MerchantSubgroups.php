<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantSubgroups
 *
 * @ORM\Table(name="merchant_subgroups", indexes={@ORM\Index(name="fk_merchant_subgroups_merchants_groups1_idx", columns={"group_id"}), @ORM\Index(name="fk_merchant_subgroups_merchants1_idx", columns={"merchant_id"})})
 * @ORM\Entity
 */
class MerchantSubgroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_id", type="integer", nullable=false)
     */
    private $merchantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="subgroup", type="string", length=45, nullable=true)
     */
    private $subgroup;

    /**
     * @var integer
     *
     * @ORM\Column(name="_order", type="integer", nullable=true)
     */
    private $order;


}

