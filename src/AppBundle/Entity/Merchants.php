<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Merchants
 *
 * @ORM\Table(name="merchants")
 * @ORM\Entity
 */
class Merchants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=200, nullable=true)
     */
    private $picture;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=200, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="locu_id", type="string", length=20, nullable=true)
     */
    private $locuId;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="partner", type="integer", nullable=true)
     */
    private $partner;

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPicture() {
        return $this->picture;
    }

    function getLogo() {
        return $this->logo;
    }

    function getLocuId() {
        return $this->locuId;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }

    function getActive() {
        return $this->active;
    }

    function getPartner() {
        return $this->partner;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPicture($picture) {
        $this->picture = $picture;
    }

    function setLogo($logo) {
        $this->logo = $logo;
    }

    function setLocuId($locuId) {
        $this->locuId = $locuId;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setPartner($partner) {
        $this->partner = $partner;
    }

}

