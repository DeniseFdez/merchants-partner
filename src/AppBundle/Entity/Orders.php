<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders", indexes={@ORM\Index(name="fk_orders_customers1_idx", columns={"customer_id"}), @ORM\Index(name="fk_orders_customers_stripe_cards1_idx", columns={"customer_stripe_card_id"}), @ORM\Index(name="fk_orders_drivers1_idx", columns={"driver_id"}), @ORM\Index(name="payment_id", columns={"payment_id"})})
 * @ORM\Entity
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_address", type="string", length=100, nullable=false)
     */
    private $deliveryAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_latitude", type="string", length=45, nullable=true)
     */
    private $deliveryLatitude;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_longitude", type="string", length=45, nullable=true)
     */
    private $deliveryLongitude;

    /**
     * @var float
     *
     * @ORM\Column(name="delivery_cost", type="float", precision=10, scale=0, nullable=false)
     */
    private $deliveryCost;

    /**
     * @var float
     *
     * @ORM\Column(name="service_fee_estimate", type="float", precision=10, scale=0, nullable=true)
     */
    private $serviceFeeEstimate;

    /**
     * @var float
     *
     * @ORM\Column(name="service_fee", type="float", precision=10, scale=0, nullable=true)
     */
    private $serviceFee;

    /**
     * @var float
     *
     * @ORM\Column(name="tip", type="float", precision=10, scale=0, nullable=true)
     */
    private $tip;

    /**
     * @var float
     *
     * @ORM\Column(name="tax_estimate", type="float", precision=10, scale=0, nullable=true)
     */
    private $taxEstimate;

    /**
     * @var float
     *
     * @ORM\Column(name="tax", type="float", precision=10, scale=0, nullable=true)
     */
    private $tax;

    /**
     * @var float
     *
     * @ORM\Column(name="total_estimate", type="float", precision=10, scale=0, nullable=false)
     */
    private $totalEstimate;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_cost", type="float", precision=10, scale=0, nullable=true)
     */
    private $ticketCost;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", precision=10, scale=0, nullable=true)
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="pex_status", type="integer", nullable=true)
     */
    private $pexStatus = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="pex_fund", type="float", precision=10, scale=0, nullable=true)
     */
    private $pexFund;

    /**
     * @var float
     *
     * @ORM\Column(name="pex_refund", type="float", precision=10, scale=0, nullable=true)
     */
    private $pexRefund;

    /**
     * @var integer
     *
     * @ORM\Column(name="used_promo", type="integer", nullable=false)
     */
    private $usedPromo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=45, nullable=true)
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=false)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=false)
     */
    private $endTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=100, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="capture_token", type="string", length=200, nullable=true)
     */
    private $captureToken;

    /**
     * @var string
     *
     * @ORM\Column(name="order_token", type="string", length=200, nullable=true)
     */
    private $orderToken;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=false)
     */
    private $paymentId = '0';

    /**
     * @var \Customers
     *
     * @ORM\ManyToOne(targetEntity="Customers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var \CustomersStripeCards
     *
     * @ORM\ManyToOne(targetEntity="CustomersStripeCards")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_stripe_card_id", referencedColumnName="id")
     * })
     */
    private $customerStripeCard;

    /**
     * @var \Drivers
     *
     * @ORM\ManyToOne(targetEntity="Drivers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * })
     */
    private $driver;


}

