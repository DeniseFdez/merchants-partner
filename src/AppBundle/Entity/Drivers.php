<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Drivers
 *
 * @ORM\Table(name="drivers", uniqueConstraints={@ORM\UniqueConstraint(name="code_driver", columns={"code"})})
 * @ORM\Entity
 */
class Drivers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=250, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_house", type="string", length=200, nullable=true)
     */
    private $telephoneHouse;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=200, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_image", type="string", length=250, nullable=true)
     */
    private $profileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="show_name", type="string", length=250, nullable=true)
     */
    private $showName;

    /**
     * @var integer
     *
     * @ORM\Column(name="receive_notifications", type="integer", nullable=true)
     */
    private $receiveNotifications;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=255, nullable=true)
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string", length=10, nullable=true)
     */
    private $deviceType;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="driver_license", type="string", length=45, nullable=true)
     */
    private $driverLicense;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=150, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_id", type="string", length=150, nullable=true)
     */
    private $stripeId;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_bank_id", type="string", length=150, nullable=true)
     */
    private $stripeBankId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="application_date", type="datetime", nullable=true)
     */
    private $applicationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acceptance_date", type="datetime", nullable=true)
     */
    private $acceptanceDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=false)
     */
    private $updatedDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=150, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=200, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=255, nullable=true)
     */
    private $bankName;

    /**
     * @var string
     *
     * @ORM\Column(name="pex_card", type="string", length=255, nullable=true)
     */
    private $pexCard;

    /**
     * @var string
     *
     * @ORM\Column(name="car", type="string", length=255, nullable=true)
     */
    private $car;

    /**
     * @var string
     *
     * @ORM\Column(name="cell", type="string", length=255, nullable=true)
     */
    private $cell;

    /**
     * @var string
     *
     * @ORM\Column(name="account_id", type="string", length=10, nullable=true)
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;


}

