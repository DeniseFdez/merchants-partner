<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderMerchants
 *
 * @ORM\Table(name="order_merchants", indexes={@ORM\Index(name="fk_order_merchants_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_order_merchants_merchants1_idx", columns={"merchant_id"})})
 * @ORM\Entity
 */
class OrderMerchants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_merchant_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderMerchantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_id", type="integer", nullable=false)
     */
    private $merchantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="delivery", type="integer", nullable=true)
     */
    private $delivery;


}

