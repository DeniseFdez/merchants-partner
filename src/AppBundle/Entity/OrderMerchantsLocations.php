<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderMerchantsLocations
 *
 * @ORM\Table(name="order_merchants_locations", indexes={@ORM\Index(name="fk_order_merchants_locations_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_order_merchants_locations_merchants1_idx", columns={"merchant_id"})})
 * @ORM\Entity
 */
class OrderMerchantsLocations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_merchant_location_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderMerchantLocationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_id", type="integer", nullable=false)
     */
    private $merchantId;

    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_location_id", type="integer", nullable=true)
     */
    private $merchantLocationId;


}

