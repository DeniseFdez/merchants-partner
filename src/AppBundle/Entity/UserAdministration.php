<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserAdministration
 *
 * @ORM\Table(name="user_administration")
 * @ORM\Entity
 */
class UserAdministration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=200, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=200, nullable=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="photofile", type="string", length=250, nullable=true)
     */
    private $photofile;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=200, nullable=true)
     */
    private $fullname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_acount", type="datetime", nullable=true)
     */
    private $createAcount;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="roll", type="integer", nullable=true)
     */
    private $roll = '1';

    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getSalt() {
        return $this->salt;
    }

    function getPhotofile() {
        return $this->photofile;
    }

    function getFullname() {
        return $this->fullname;
    }

    function getLastLogin() {
        return $this->lastLogin;
    }

    function getCreateAcount() {
        return $this->createAcount;
    }

    function getActive() {
        return $this->active;
    }

    function getEmail() {
        return $this->email;
    }

    function getRoll() {
        return $this->roll;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setSalt($salt) {
        $this->salt = $salt;
    }

    function setPhotofile($photofile) {
        $this->photofile = $photofile;
    }

    function setFullname($fullname) {
        $this->fullname = $fullname;
    }

    function setLastLogin(\DateTime $lastLogin) {
        $this->lastLogin = $lastLogin;
    }

    function setCreateAcount(\DateTime $createAcount) {
        $this->createAcount = $createAcount;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setRoll($roll) {
        $this->roll = $roll;
    }


}

