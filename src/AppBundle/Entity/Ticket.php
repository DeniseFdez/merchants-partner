<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 *
 * @ORM\Table(name="ticket", uniqueConstraints={@ORM\UniqueConstraint(name="code", columns={"code"})}, indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="driver_id", columns={"driver_id"}), @ORM\Index(name="code_2", columns={"code"})})
 * @ORM\Entity
 */
class Ticket
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer", nullable=false)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_time", type="datetime", nullable=true)
     */
    private $dateTime = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="person_response", type="string", length=200, nullable=true)
     */
    private $personResponse;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_client_response", type="datetime", nullable=true)
     */
    private $dateClientResponse;

    /**
     * @var string
     *
     * @ORM\Column(name="response_client", type="text", length=65535, nullable=true)
     */
    private $responseClient;

    /**
     * @var string
     *
     * @ORM\Column(name="coment_client", type="text", length=65535, nullable=true)
     */
    private $comentClient;

    /**
     * @var string
     *
     * @ORM\Column(name="client_email", type="string", length=200, nullable=true)
     */
    private $clientEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="client_name", type="string", length=200, nullable=true)
     */
    private $clientName;

    /**
     * @var integer
     *
     * @ORM\Column(name="driver_id", type="integer", nullable=true)
     */
    private $driverId;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="type_ticket", type="integer", nullable=false)
     */
    private $typeTicket = '0';


}

