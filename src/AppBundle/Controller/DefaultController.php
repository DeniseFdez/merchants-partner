<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

class DefaultController extends FOSRestController {

    /**
     * @param string $$user email from merchant (Test: javierfer001@gmail.com)
     * @param string $password  Description (Test: 123456)
     * 
     * @Rest\Post("/login")
     */
    public function postLoginAction(Request $request) {
        $user=$request->get('user');
        $pass=$request->get('password');
        if ($user === null || $pass===null) {
            return View("User or Password incorrect", Response::HTTP_NOT_FOUND);
        }
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Merchants')->findOneBy(array("email"=>$user,"password"=>$pass));
        if ($restresult === null) {
            return View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     *@param integer $merchant_id (Test: 530) 
     * 
     * @Rest\Post("/groups")
     *
     */
    public function listMerchantGroupAction(Request $request) {
        $merchant = $request->get('merchant_id');
        
        $data = $this->getDoctrine()->getRepository('AppBundle:MerchantsGroups')->findBy(array("merchant"=>$merchant));
        
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }

    /**
     * @param integer $group_id (Test: 218) 
     * 
     * @Rest\Post("/product/for/groups")
     */
    public function listProductForGroupsAction(Request $request) {
        $group_id = $request->get('group_id');
        
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:MerchantProducts')->findBy(array("group"=>$group_id));
     
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }
    
    /**
     * @param integer $product_id (Test: 7197)
     * 
     * @Rest\Post("/extra/for/product")
     */
    public function listExtraItemsForProductAction(Request $request) {
        $product_id = $request->get('product_id');
        
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('AppBundle:MerchantProductsExtrasGroups')->findBy(array("product"=>$product_id));
     
        $view = $this->view($data, Response::HTTP_INTERNAL_SERVER_ERROR);
        return $view;
    }
    
}
