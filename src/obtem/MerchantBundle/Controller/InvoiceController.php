<?php

namespace obtem\MerchantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class InvoiceController extends Controller {

    public function invoiceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        $merchantId = $user;
        $invoices = $em->getRepository('ApiBundle:Invoice')->findBy(array('merchant' => $merchantId));
        
        return $this->render('MerchantBundle:Invoice:invoice.html.twig', array(
                    "invoices" => $invoices,
        ));
    }

    public function ordersInvoiceAction(Request $request,$invoiceId) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        $merchantId = $user; 
        
        $invoiceDetail = $em->getRepository('ApiBundle:Invoice')->getInvoiceOrders($merchantId,$invoiceId);

        return $this->render('MerchantBundle:Invoice:ordersinvoice.html.twig', array(
                    "invoiceDetail" => $invoiceDetail
        ));
    }

}
