<?php

namespace obtem\MerchantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ProductsController extends Controller {

    public function productAction(Request $request, $groupId ) {
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository('ApiBundle:MerchantProducts')->findBy(array("group" => $groupId));

        return $this->render('MerchantBundle:Products:products.html.twig', array(
                    'products' => $products,
                    'groupSellect' => $groupId
        ));
    }

    public function dataAction(Request $request, $groupId, $productId) {
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ApiBundle:MerchantProducts')->find($productId);
//        $description = $product->getDescription();

        return $this->render('MerchantBundle:Products:data.html.twig', array(
                    'product' => $product,
                    'groupSellect' => $groupId,
//            'description' => $description,
        ));
    }

    public function createProductAction(Request $request, $groupId, $productId) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        if ($request->isMethod("POST")) {
            if ($productId > 0) {
                $product = $em->getRepository('ApiBundle:MerchantProducts')->find($productId);
                $product->setName($request->get("name"));
                $product->setDescription($request->get("description"));
                $product->setPrice($request->get("price"));

                $em->persist($product);
                $em->flush();
            } else {
                $group = $em->getRepository('ApiBundle:MerchantsGroups')->find($groupId);
                $merchant = $em->getRepository('ApiBundle:Merchants')->find($merchantId);

                $newProduct = new \obtem\ApiBundle\Entity\MerchantProducts();
                $newProduct->setName($request->get("name"));
                $newProduct->setDescription($request->get("description"));
                $newProduct->setPrice($request->get("price"));
                $newProduct->setMerchant($merchant);
                $newProduct->setGroup($group);
                $newProduct->setRestricted(0);
                $newProduct->setActive(1);
                $newProduct->setAllDay(1);

                $em->persist($newProduct);
                $em->flush();
            }
            return $this->redirect($this->generateUrl("products", array('groupId' => $groupId)));
            //return $this->productAction($groupId);
        }
    }

    public function activateProductAction(Request $request, $groupId) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get("key");
        $activeValue = $request->get("active");

        $product = $em->getRepository('ApiBundle:MerchantProducts')->find($id);

        if ($activeValue == 1) {
            $activeValue = 0;
        } else {
            $activeValue = 1;
        }
        $product->setActive($activeValue);
        $em->persist($product);
        $em->flush();

        return $this->redirect($this->generateUrl("products", array('groupId' => $groupId)));
        //return $this->productAction($groupId);
    }

    public function extraAction(Request $request, $productId) {
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ApiBundle:MerchantProducts')->find($productId);
        $group = $product->getGroup();
        $groupId = $group->getId();
        $extras = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->findBy(array("product" => $product));

        return $this->render('MerchantBundle:Products:extras.html.twig', array(
                    'extras' => $extras,
                    'product' => $productId,
                    'group' => $groupId,
        ));
    }

    public function activateExtraAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get("key");
        $activeValue = $request->get("active");
        $extra = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->find($id);
        $product = $extra->getProduct();
        if ($activeValue == 1) {
            $activeValue = 0;
        } else {
            $activeValue = 1;
        }
        $extra->setActive($activeValue);
        $em->persist($extra);
        $em->flush();
        
        return $this->redirect($this->generateUrl("product_extras", array('productId' => $product->getId())));
        //return $this->extraAction($product->getId());
    }

    public function dataExtraAction(Request $request, $extraId, $productId) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $extra = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->find($extraId);

        return $this->render('MerchantBundle:Products:dataextra.html.twig', array(
                    'extra' => $extra,
                    'product' => $productId,
        ));
    }

    public function createExtraAction(Request $request, $extraId) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $product = $request->get("prodId");

        if ($request->get("order") == "true") {
            $orderValue = 1;
        } else {
            $orderValue = 0;
        }
        if ($request->get("requeride") == "true") {
            $requeValue = 1;
        } else {
            $requeValue = 0;
        }

        if ($request->isMethod("POST")) {
            if ($extraId > 0) {
                $extra = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->find($extraId);
                $extra->setGroup($request->get("group"));
                $extra->setCantFreeExtra($request->get("freeExtra"));
                $extra->setOrder($orderValue);
                $extra->setRequired($requeValue);
                $em->persist($extra);
                $em->flush();

                $productExtra = $em->getRepository('ApiBundle:MerchantProducts')->find($product);
            } else {
                $productExtra = $em->getRepository('ApiBundle:MerchantProducts')->find($product);
                $merchant = $em->getRepository('ApiBundle:Merchants')->find($merchantId);

                $newExtra = new \obtem\ApiBundle\Entity\MerchantProductsExtrasGroups();
                $newExtra->setGroup($request->get("group"));
                $newExtra->setCantFreeExtra($request->get("freeExtra"));
                $newExtra->setOrder($orderValue);
                $newExtra->setRequired($requeValue);
                $newExtra->setProduct($productExtra);
                $newExtra->setMerchant($merchant);
                $em->persist($newExtra);
                $em->flush();
            }
            return $this->redirect($this->generateUrl("product_extras", array('productId' => $product)));
            //return $this->extraAction($product);
        }
    }

    public function itemsAction(Request $request, $productId, $extraId) {
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $em = $this->getDoctrine()->getManager();
        $items = $em->getRepository('ApiBundle:MerchantProductsExtrasItem')->findBy(array("group" => $extraId));
        $extra = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->find($extraId);

        return $this->render('MerchantBundle:Products:items.html.twig', array(
                    'items' => $items,
                    'product' => $productId,
                    'extra' => $extra,
        ));
    }

    public function dataItemAction(Request $request, $productId, $extraId, $itemId) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $items = $em->getRepository('ApiBundle:MerchantProductsExtrasItem')->find($itemId);

        return $this->render('MerchantBundle:Products:dataitem.html.twig', array(
                    'extra' => $extraId,
                    'product' => $productId,
                    'items' => $items,
        ));
    }

    public function editItemAction(Request $request, $itemId, $extraId) {

        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user == 'none') {
            return $this->redirect($this->generateUrl("merchant_login"));
        }
        $extra = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->find($extraId);

        $productId = $extra->getProduct();
        if ($request->get("order") == "true") {
            $orderValue = 1;
        } else {
            $orderValue = 0;
        }
        if ($request->isMethod("POST")) {
            if ($itemId > 0) {
                $item = $em->getRepository('ApiBundle:MerchantProductsExtrasItem')->find($itemId);
                $item->setName($request->get("name"));
                $item->setPrice($request->get("price"));
                $item->setOrder($orderValue);
                $em->persist($item);
                $em->flush();
            } else {
                $extra = $em->getRepository('ApiBundle:MerchantProductsExtrasGroups')->find($extraId);
                $product = $em->getRepository('ApiBundle:MerchantProducts')->find($productId);
                $newItem = new \obtem\ApiBundle\Entity\MerchantProductsExtrasItem ();
                $newItem->setName($request->get("name"));
                $newItem->setPrice($request->get("price"));
                $newItem->setOrder($orderValue);
                $newItem->setProduct($product);
                $newItem->setGroup($extra);
                $em->persist($newItem);
                $em->flush();
            }
            return $this->redirect($this->generateUrl("extras_item", array('productId' => $productId, 'extraId' => $extraId)));
            //return $this->itemsAction($productId, $extraId);
        }
    }

    public function activateItemAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get("key");
        $activeValue = $request->get("active");

        $item = $em->getRepository('ApiBundle:MerchantProductsExtrasItem')->find($id);
        $extra = $item->getGroup();
        $product = $item->getProduct();
        if ($activeValue == 1) {
            $activeValue = 0;
        } else {
            $activeValue = 1;
        }
        $item->setActive($activeValue);

        $em->persist($item);
        $em->flush();

        return $this->redirect($this->generateUrl("extras_item", array('productId' => $product->getId(), 'extraId' => $extra->getId())));
        //return $this->itemsAction($product->getId(), $extra->getId());
    }

}
