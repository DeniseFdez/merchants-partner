<?php

namespace obtem\MerchantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DashboardController extends Controller {

    public function dashboardAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
            $orders = $em->getRepository('ApiBundle:Orders')->getNewOrders($user);
            $orderproducts = array();
            $orderhistory = $em->getRepository('ApiBundle:Orders')->getAllOrders($user);

            return $this->render('MerchantBundle:Default:dashboard.html.twig', array('orders' => $orders, 'orderproducts' => $orderproducts, 'orderhistory' => $orderhistory));
        }
        return $this->redirect($this->generateUrl("merchant_login"));
    }

    public function orderProductsAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
            $orderproducts = $em->getRepository('ApiBundle:Orders')->getOrderProducts($request->get("key"), $user);

            $ordercomment = $em->getRepository('ApiBundle:Orders')->find($request->get("key"));

            return $this->render('MerchantBundle:Products:dashboarDetail.html.twig', array('orders' => $orderproducts, 'orderComment' => $ordercomment->getComments()));
            //return $this->render('MerchantBundle:Default:dashboard.html.twig', array('orderproducts' => $orderproducts,));
        }
        return $this->redirect($this->generateUrl("merchant_login"));
    }

}

//return new \Symfony\Component\HttpFoundation\Response ($orden);