<?php

namespace obtem\MerchantBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultController extends Controller {

    public function indexAction(Request $request) {
        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
            return $this->render('MerchantBundle:Default:index.html.twig');
        }
        return $this->loginAction($request);
    }

    public function loginAction(Request $request) {
        $error_login = "";
        $email = "";
        $request->getSession()->set("user_login", 'none');
        if ($request->getMethod() == "POST") {

            $email = $request->get('email');
            $pass = $request->get('password');

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('ApiBundle:Merchants')->findOneBy(array('email' => $email, 'password' => $pass));

            if ($user != null) {
                // Logueamos al usuario
                $request->getSession()->set("user_login", $user->getId());

                return $this->redirect($this->generateUrl("homepage"));
            }
            $error_login = "No existe el usuario o password incorrecto";
        }
        return $this->render('MerchantBundle:Default:login.html.twig', array(
                    'error' => $error_login,
                    'email' => $email
        ));
    }

    public function menuAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
            $entities = $em->getRepository('ApiBundle:MerchantsGroups')->findBy(array("merchant" => $user));

            return $this->render('MerchantBundle:Default:menu.html.twig', array(
                        "entities" => $entities
            ));
        }
        return $this->loginAction($request);
    }

    public function supportAction(Request $request) {
        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
            return $this->render('MerchantBundle:Default:support.html.twig');
        }
        return $this->loginAction($request);
    }

    public function settingAction(Request $request) {
        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
            return $this->render('MerchantBundle:Default:setting.html.twig');
        }
        return $this->loginAction($request);
    }

    public function changePasswordAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $request->getSession()->get("user_login");
        if ($user != 'none') {
        $password = $request->get("password");
        $merchant = $em->getRepository('ApiBundle:Merchants')->find($user);
        $merchant->setPassword($password);

        $em->persist($merchant);
        $em->flush();
        
        return new \Symfony\Component\HttpFoundation\JsonResponse("OK");
        }
        return $this->loginAction($request);
    }

    public function cardAction() {
        return $this->render('MerchantBundle:Default:card.html.twig');
    }

    public function sendMailAction(Request $request) {
        if ($request->isMethod("POST")) {
            $name = $request->get("name");
            $phone = $request->get("phone");
            $message = $request->get("message");
            $message_ = "Phone : " . $phone . " name : " . $name . " message : " . $message;

            $message = \Swift_Message::newInstance()
                    ->setSubject('Obtem-Merchant Support')
                    ->setFrom('support@obtem.com')
                    ->setTo('denise.fdez@gmail.com')
                    ->setBody(" " . $message_);

            $this->get('mailer')->send($message);
        }
        return $this->supportAction();
    }

}
