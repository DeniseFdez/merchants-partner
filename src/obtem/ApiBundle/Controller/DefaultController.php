<?php

namespace obtem\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;

class DefaultController  extends Controller 
{
    public function indexAction()
    {
        return new Response("OK");
    }
}
