<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantProductsExtrasItem
 *
 * @ORM\Table(name="merchant_products_extras_item", indexes={@ORM\Index(name="fk_merchant_products_extras_item_merchant_products_extras_g_idx", columns={"group_id"}), @ORM\Index(name="fk_merchant_products_extras_item_merchant_products1_idx", columns={"product_id"})})
 * @ORM\Entity
 */
class MerchantProductsExtrasItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="_order", type="integer", nullable=false)
     */
    private $order = '1';

    /**
     * @var \MerchantProducts
     *
     * @ORM\ManyToOne(targetEntity="MerchantProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \MerchantProductsExtrasGroups
     *
     * @ORM\ManyToOne(targetEntity="MerchantProductsExtrasGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * })
     */
    private $group;

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getPrice() {
        return $this->price;
    }

    function getActive() {
        return $this->active;
    }

    function getOrder() {
        return $this->order;
    }

    function getProduct() {
        return $this->product;
    }

    function getGroup() {
        return $this->group;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setOrder($order) {
        $this->order = $order;
    }

    function setProduct($product) {
        $this->product = $product;
    }

    function setGroup($group) {
        $this->group = $group;
    }


}

