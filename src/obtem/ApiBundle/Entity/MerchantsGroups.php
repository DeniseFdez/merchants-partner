<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantsGroups
 *
 * @ORM\Table(name="merchants_groups", indexes={@ORM\Index(name="fk_merchants_groups_merchants1_idx", columns={"merchant_id"})})
 * @ORM\Entity
 */
class MerchantsGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="_group", type="string", length=45, nullable=true)
     */
    private $group;

    /**
     * @var integer
     *
     * @ORM\Column(name="_order", type="integer", nullable=true)
     */
    private $order = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Merchants
     *
     * @ORM\ManyToOne(targetEntity="Merchants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     * })
     */
    private $merchant;

    function getId() {
        return $this->id;
    }

    function getGroup() {
        return $this->group;
    }

    function getOrder() {
        return $this->order;
    }

    function getActive() {
        return $this->active;
    }

    function getMerchant() {
        return $this->merchant;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroup($group) {
        $this->group = $group;
    }

    function setOrder($order) {
        $this->order = $order;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setMerchant($merchant) {
        $this->merchant = $merchant;
    }


}

