<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDriversStars
 *
 * @ORM\Table(name="order_drivers_stars", indexes={@ORM\Index(name="fk_order_drivers_stars_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_order_drivers_stars_drivers1_idx", columns={"driver_id"})})
 * @ORM\Entity
 */
class OrderDriversStars
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_driver_stars_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderDriverStarsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="driver_id", type="integer", nullable=false)
     */
    private $driverId;

    /**
     * @var integer
     *
     * @ORM\Column(name="stars", type="integer", nullable=true)
     */
    private $stars;


}

