<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupsI18n
 *
 * @ORM\Table(name="groups_i18n", indexes={@ORM\Index(name="fk_groups_i18n_merchants_groups1_idx", columns={"group_id"})})
 * @ORM\Entity
 */
class GroupsI18n
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=45, nullable=true)
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="_group", type="string", length=45, nullable=true)
     */
    private $group;

    /**
     * @var \MerchantsGroups
     *
     * @ORM\ManyToOne(targetEntity="MerchantsGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * })
     */
    private $group2;


}

