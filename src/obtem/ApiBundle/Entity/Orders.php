<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders", indexes={@ORM\Index(name="fk_orders_customers1_idx", columns={"customer_id"}), @ORM\Index(name="fk_orders_customers_stripe_cards1_idx", columns={"customer_stripe_card_id"}), @ORM\Index(name="fk_orders_drivers1_idx", columns={"driver_id"}), @ORM\Index(name="payment_id", columns={"payment_id"}), @ORM\Index(name="invoice_id", columns={"invoice_id"})})
 * @ORM\Entity(repositoryClass="obtem\ApiBundle\Entity\GeneralRepository")
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="driver_id", type="integer", nullable=false)
     */
    private $driverId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_stripe_card_id", type="integer", nullable=false)
     */
    private $customerStripeCardId;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_address", type="string", length=100, nullable=false)
     */
    private $deliveryAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_latitude", type="string", length=45, nullable=true)
     */
    private $deliveryLatitude;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_longitude", type="string", length=45, nullable=true)
     */
    private $deliveryLongitude;

    /**
     * @var float
     *
     * @ORM\Column(name="delivery_cost", type="float", precision=10, scale=0, nullable=false)
     */
    private $deliveryCost;

    /**
     * @var float
     *
     * @ORM\Column(name="service_fee_estimate", type="float", precision=10, scale=0, nullable=true)
     */
    private $serviceFeeEstimate;

    /**
     * @var float
     *
     * @ORM\Column(name="service_fee", type="float", precision=10, scale=0, nullable=true)
     */
    private $serviceFee;

    /**
     * @var float
     *
     * @ORM\Column(name="tip", type="float", precision=10, scale=0, nullable=true)
     */
    private $tip;

    /**
     * @var float
     *
     * @ORM\Column(name="tax_estimate", type="float", precision=10, scale=0, nullable=true)
     */
    private $taxEstimate;

    /**
     * @var float
     *
     * @ORM\Column(name="tax", type="float", precision=10, scale=0, nullable=true)
     */
    private $tax;

    /**
     * @var float
     *
     * @ORM\Column(name="total_estimate", type="float", precision=10, scale=0, nullable=false)
     */
    private $totalEstimate;

    /**
     * @var float
     *
     * @ORM\Column(name="ticket_cost", type="float", precision=10, scale=0, nullable=true)
     */
    private $ticketCost;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", precision=10, scale=0, nullable=true)
     */
    private $total;

    /**
     * @var integer
     *
     * @ORM\Column(name="pex_status", type="integer", nullable=true)
     */
    private $pexStatus = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="pex_fund", type="float", precision=10, scale=0, nullable=true)
     */
    private $pexFund;

    /**
     * @var float
     *
     * @ORM\Column(name="pex_refund", type="float", precision=10, scale=0, nullable=true)
     */
    private $pexRefund;

    /**
     * @var integer
     *
     * @ORM\Column(name="used_promo", type="integer", nullable=false)
     */
    private $usedPromo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=45, nullable=true)
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=false)
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=false)
     */
    private $endTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=100, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="capture_token", type="string", length=200, nullable=true)
     */
    private $captureToken;

    /**
     * @var string
     *
     * @ORM\Column(name="order_token", type="string", length=200, nullable=true)
     */
    private $orderToken;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_id", type="integer", nullable=true)
     */
    private $paymentId = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="invoice_id", type="integer", nullable=true)
     */
    private $invoiceId = '0';


     /**
     * @var \Customers
     *
     * @ORM\ManyToOne(targetEntity="Customers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;

    /**
     * @var \CustomersStripeCards
     *
     * @ORM\ManyToOne(targetEntity="CustomersStripeCards")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_stripe_card_id", referencedColumnName="id")
     * })
     */
    private $customerStripeCard;

    /**
     * @var \Drivers
     *
     * @ORM\ManyToOne(targetEntity="Drivers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="driver_id", referencedColumnName="id")
     * })
     */
    private $driver;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set driverId
     *
     * @param integer $driverId
     *
     * @return Orders
     */
    public function setDriverId($driverId)
    {
        $this->driverId = $driverId;

        return $this;
    }

    /**
     * Get driverId
     *
     * @return integer
     */
    public function getDriverId()
    {
        return $this->driverId;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return Orders
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set customerStripeCardId
     *
     * @param integer $customerStripeCardId
     *
     * @return Orders
     */
    public function setCustomerStripeCardId($customerStripeCardId)
    {
        $this->customerStripeCardId = $customerStripeCardId;

        return $this;
    }

    /**
     * Get customerStripeCardId
     *
     * @return integer
     */
    public function getCustomerStripeCardId()
    {
        return $this->customerStripeCardId;
    }

    /**
     * Set deliveryAddress
     *
     * @param string $deliveryAddress
     *
     * @return Orders
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return string
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Set deliveryLatitude
     *
     * @param string $deliveryLatitude
     *
     * @return Orders
     */
    public function setDeliveryLatitude($deliveryLatitude)
    {
        $this->deliveryLatitude = $deliveryLatitude;

        return $this;
    }

    /**
     * Get deliveryLatitude
     *
     * @return string
     */
    public function getDeliveryLatitude()
    {
        return $this->deliveryLatitude;
    }

    /**
     * Set deliveryLongitude
     *
     * @param string $deliveryLongitude
     *
     * @return Orders
     */
    public function setDeliveryLongitude($deliveryLongitude)
    {
        $this->deliveryLongitude = $deliveryLongitude;

        return $this;
    }

    /**
     * Get deliveryLongitude
     *
     * @return string
     */
    public function getDeliveryLongitude()
    {
        return $this->deliveryLongitude;
    }

    /**
     * Set deliveryCost
     *
     * @param float $deliveryCost
     *
     * @return Orders
     */
    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;

        return $this;
    }

    /**
     * Get deliveryCost
     *
     * @return float
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }

    /**
     * Set serviceFeeEstimate
     *
     * @param float $serviceFeeEstimate
     *
     * @return Orders
     */
    public function setServiceFeeEstimate($serviceFeeEstimate)
    {
        $this->serviceFeeEstimate = $serviceFeeEstimate;

        return $this;
    }

    /**
     * Get serviceFeeEstimate
     *
     * @return float
     */
    public function getServiceFeeEstimate()
    {
        return $this->serviceFeeEstimate;
    }

    /**
     * Set serviceFee
     *
     * @param float $serviceFee
     *
     * @return Orders
     */
    public function setServiceFee($serviceFee)
    {
        $this->serviceFee = $serviceFee;

        return $this;
    }

    /**
     * Get serviceFee
     *
     * @return float
     */
    public function getServiceFee()
    {
        return $this->serviceFee;
    }

    /**
     * Set tip
     *
     * @param float $tip
     *
     * @return Orders
     */
    public function setTip($tip)
    {
        $this->tip = $tip;

        return $this;
    }

    /**
     * Get tip
     *
     * @return float
     */
    public function getTip()
    {
        return $this->tip;
    }

    /**
     * Set taxEstimate
     *
     * @param float $taxEstimate
     *
     * @return Orders
     */
    public function setTaxEstimate($taxEstimate)
    {
        $this->taxEstimate = $taxEstimate;

        return $this;
    }

    /**
     * Get taxEstimate
     *
     * @return float
     */
    public function getTaxEstimate()
    {
        return $this->taxEstimate;
    }

    /**
     * Set tax
     *
     * @param float $tax
     *
     * @return Orders
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set totalEstimate
     *
     * @param float $totalEstimate
     *
     * @return Orders
     */
    public function setTotalEstimate($totalEstimate)
    {
        $this->totalEstimate = $totalEstimate;

        return $this;
    }

    /**
     * Get totalEstimate
     *
     * @return float
     */
    public function getTotalEstimate()
    {
        return $this->totalEstimate;
    }

    /**
     * Set ticketCost
     *
     * @param float $ticketCost
     *
     * @return Orders
     */
    public function setTicketCost($ticketCost)
    {
        $this->ticketCost = $ticketCost;

        return $this;
    }

    /**
     * Get ticketCost
     *
     * @return float
     */
    public function getTicketCost()
    {
        return $this->ticketCost;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Orders
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set pexStatus
     *
     * @param integer $pexStatus
     *
     * @return Orders
     */
    public function setPexStatus($pexStatus)
    {
        $this->pexStatus = $pexStatus;

        return $this;
    }

    /**
     * Get pexStatus
     *
     * @return integer
     */
    public function getPexStatus()
    {
        return $this->pexStatus;
    }

    /**
     * Set pexFund
     *
     * @param float $pexFund
     *
     * @return Orders
     */
    public function setPexFund($pexFund)
    {
        $this->pexFund = $pexFund;

        return $this;
    }

    /**
     * Get pexFund
     *
     * @return float
     */
    public function getPexFund()
    {
        return $this->pexFund;
    }

    /**
     * Set pexRefund
     *
     * @param float $pexRefund
     *
     * @return Orders
     */
    public function setPexRefund($pexRefund)
    {
        $this->pexRefund = $pexRefund;

        return $this;
    }

    /**
     * Get pexRefund
     *
     * @return float
     */
    public function getPexRefund()
    {
        return $this->pexRefund;
    }

    /**
     * Set usedPromo
     *
     * @param integer $usedPromo
     *
     * @return Orders
     */
    public function setUsedPromo($usedPromo)
    {
        $this->usedPromo = $usedPromo;

        return $this;
    }

    /**
     * Get usedPromo
     *
     * @return integer
     */
    public function getUsedPromo()
    {
        return $this->usedPromo;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Orders
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Orders
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return Orders
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return Orders
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Orders
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set captureToken
     *
     * @param string $captureToken
     *
     * @return Orders
     */
    public function setCaptureToken($captureToken)
    {
        $this->captureToken = $captureToken;

        return $this;
    }

    /**
     * Get captureToken
     *
     * @return string
     */
    public function getCaptureToken()
    {
        return $this->captureToken;
    }

    /**
     * Set orderToken
     *
     * @param string $orderToken
     *
     * @return Orders
     */
    public function setOrderToken($orderToken)
    {
        $this->orderToken = $orderToken;

        return $this;
    }

    /**
     * Get orderToken
     *
     * @return string
     */
    public function getOrderToken()
    {
        return $this->orderToken;
    }

    /**
     * Set paymentId
     *
     * @param integer $paymentId
     *
     * @return Orders
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return integer
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set invoiceId
     *
     * @param integer $invoiceId
     *
     * @return Orders
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }
    
    function getCustomer() {
        return $this->customer;
    }

    function getCustomerStripeCard() {
        return $this->customerStripeCard;
    }

    function getDriver() {
        return $this->driver;
    }

    function setCustomer( $customer) {
        $this->customer = $customer;
    }

    function setCustomerStripeCard( $customerStripeCard) {
        $this->customerStripeCard = $customerStripeCard;
    }

    function setDriver($driver) {
        $this->driver = $driver;
    }

    public function _showStatus() {
        switch ($this->status) {
            case 1: return "Received";
            case 2: return "Picked Up";
            case 3: return "Delivered";
            case 4: return "Cancelled";
            case 5: return "Cancelled";
        }
        return "Received";
    }
    
}
