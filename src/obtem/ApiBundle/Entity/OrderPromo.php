<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderPromo
 *
 * @ORM\Table(name="order_promo", indexes={@ORM\Index(name="fk_order_promo_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_order_promo_customers_promocodes1_idx", columns={"promocode_id"}), @ORM\Index(name="fk_order_promo_customers1_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class OrderPromo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="promocode_id", type="integer", nullable=false)
     */
    private $promocodeId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="used_date", type="datetime", nullable=true)
     */
    private $usedDate;


}

