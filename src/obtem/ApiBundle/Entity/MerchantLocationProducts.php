<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantLocationProducts
 *
 * @ORM\Table(name="merchant_location_products", indexes={@ORM\Index(name="fk_merchant_location_products_merchant_products1_idx", columns={"product_id"}), @ORM\Index(name="fk_merchant_location_products_merchant_locations1_idx", columns={"location_id"})})
 * @ORM\Entity
 */
class MerchantLocationProducts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="location_id", type="integer", nullable=false)
     */
    private $locationId;

    /**
     * @var \MerchantProducts
     *
     * @ORM\ManyToOne(targetEntity="MerchantProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;


}

