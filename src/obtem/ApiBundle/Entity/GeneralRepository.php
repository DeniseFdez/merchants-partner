<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\EntityRepository;
/**
 * Description of GeneralRepository
 *
 * @author obtem
 */
class GeneralRepository extends EntityRepository{
    
    public function getNewOrders($userid){
        //$dql = "SELECT orders,m FROM ApiBundle:Orders orders JOIN purchase.purchasesprod pp LEFT JOIN pp.product product where purchase.status=0 and purchase.user=:user_id ";
        
        $dql = "SELECT orders.id as idorder,orders.startTime,orders.totalEstimate, orders.comments,drivers.fullName,drivers.profileImage "
                . " FROM ApiBundle:Orders orders JOIN orders.driver drivers "
                . " JOIN ApiBundle:OrderMerchants omerchant "
                . " WHERE orders.id = omerchant.orderId and orders.status=1 and omerchant.merchantId=:userid order by orders.startTime desc";
        
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter("userid", $userid);
        return $query->getResult();
    }
    
    public function getOrderProducts($orderid,$userid){
        
        $dql = "SELECT oi.productId,mp.name, oi.qty, mpeg.group extraname, mpei.name AS itemname "
                . "FROM ApiBundle:MerchantProducts mp, ApiBundle:OrderItems oi LEFT OUTER JOIN ApiBundle:OrderItemsExtras oie with oie.orderId = oi.orderId and oie.productId=oi.productId "
                . "LEFT OUTER JOIN ApiBundle:MerchantProductsExtrasGroups mpeg with oie.groupId=mpeg.id "
                . "LEFT OUTER JOIN ApiBundle:MerchantProductsExtrasItem mpei with mpei.id=oie.extraId "
                . "WHERE oi.orderId = :orderid and oi.merchantId=:userid and oi.productId=mp.id";
        
        //select oi.product_id,mp.name, oi.qty, oi.price FROM order_items oi, merchant_products mp where oi.merchant_id=504 and oi.order_id=1158 and oi.product_id=mp.id
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter("orderid", $orderid);
        $query->setParameter("userid", $userid);
        
        return $query->getResult();
    }
    
    public function getAllOrders($userid){
        //$dql = "SELECT orders,m FROM ApiBundle:Orders orders JOIN purchase.purchasesprod pp LEFT JOIN pp.product product where purchase.status=0 and purchase.user=:user_id ";
        
        $dql = "SELECT orders "
                . " FROM ApiBundle:Orders orders JOIN orders.driver drivers "
                . " JOIN ApiBundle:OrderMerchants omerchant "
                . " WHERE orders.id = omerchant.orderId and omerchant.merchantId=:userid order by orders.startTime desc";
        
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter("userid", $userid);
        return $query->getResult();
    }
    
        public function getInvoiceOrders($userid, $invoiceId){
        
        
        $dql = "SELECT orders "
                . " FROM ApiBundle:Orders orders JOIN orders.driver drivers "
                . " JOIN ApiBundle:OrderMerchants omerchant "
                . " WHERE orders.id = omerchant.orderId and omerchant.merchantId=:userid and orders.invoiceId =:invoiceid";
        
        $query=$this->getEntityManager()->createQuery($dql);
        $query->setParameter("userid", $userid);
        $query->setParameter("invoiceid", $invoiceId);
        return $query->getResult();
    }
    
}

