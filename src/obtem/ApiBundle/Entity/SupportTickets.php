<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupportTickets
 *
 * @ORM\Table(name="support_tickets", indexes={@ORM\Index(name="fk_support_tickets_customers1_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class SupportTickets
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_created", type="datetime", nullable=false)
     */
    private $dateCreated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_updated", type="datetime", nullable=true)
     */
    private $dateUpdated;


}

