<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupportTicketsLog
 *
 * @ORM\Table(name="support_tickets_log", indexes={@ORM\Index(name="fk_support_tickets_log_support_tickets1_idx", columns={"ticket_id"})})
 * @ORM\Entity
 */
class SupportTicketsLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ticket_id", type="integer", nullable=false)
     */
    private $ticketId;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;


}

