<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesMerchants
 *
 * @ORM\Table(name="categories_merchants", indexes={@ORM\Index(name="fk_categories_merchants_categories1_idx", columns={"category_id"}), @ORM\Index(name="fk_categories_merchants_merchants1_idx", columns={"merchant_id"})})
 * @ORM\Entity
 */
class CategoriesMerchants
{
    /**
     * @var integer
     *
     * @ORM\Column(name="category_merchant_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoryMerchantId;

    /**
     * @var \Categories
     *
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Merchants
     *
     * @ORM\ManyToOne(targetEntity="Merchants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     * })
     */
    private $merchant;


}

