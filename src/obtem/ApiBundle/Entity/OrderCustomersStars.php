<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderCustomersStars
 *
 * @ORM\Table(name="order_customers_stars", indexes={@ORM\Index(name="fk_orders_customers_stars_orders1_idx", columns={"order_id"}), @ORM\Index(name="fk_orders_customers_stars_customers1_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class OrderCustomersStars
{
    /**
     * @var integer
     *
     * @ORM\Column(name="order_customer_stars_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderCustomerStarsId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    private $customerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="stars", type="integer", nullable=true)
     */
    private $stars;


}

