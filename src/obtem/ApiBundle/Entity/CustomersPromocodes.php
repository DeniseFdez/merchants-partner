<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomersPromocodes
 *
 * @ORM\Table(name="customers_promocodes", indexes={@ORM\Index(name="fk_customers_promocodes_customers1_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class CustomersPromocodes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="promocode", type="string", length=45, nullable=true)
     */
    private $promocode;

    /**
     * @var \Customers
     *
     * @ORM\ManyToOne(targetEntity="Customers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    private $customer;


}

