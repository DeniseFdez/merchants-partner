<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantProductsExtrasGroups
 *
 * @ORM\Table(name="merchant_products_extras_groups", indexes={@ORM\Index(name="fk_merchant_products_extras_groups_merchants1_idx", columns={"merchant_id"}), @ORM\Index(name="product_id", columns={"product_id"})})
 * @ORM\Entity
 */
class MerchantProductsExtrasGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="_group", type="string", length=50, nullable=false)
     */
    private $group;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="cant_free_extra", type="integer", nullable=true)
     */
    private $cantFreeExtra = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="required", type="integer", nullable=false)
     */
    private $required = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="_order", type="integer", nullable=false)
     */
    private $order = '1';

    /**
     * @var \Merchants
     *
     * @ORM\ManyToOne(targetEntity="Merchants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="merchant_id", referencedColumnName="id")
     * })
     */
    private $merchant;

    /**
     * @var \MerchantProducts
     *
     * @ORM\ManyToOne(targetEntity="MerchantProducts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    function getId() {
        return $this->id;
    }

    function getGroup() {
        return $this->group;
    }

    function getActive() {
        return $this->active;
    }

    function getCantFreeExtra() {
        return $this->cantFreeExtra;
    }

    function getRequired() {
        return $this->required;
    }

    function getOrder() {
        return $this->order;
    }

    function getMerchant() {
        return $this->merchant;
    }

    function getProduct() {
        return $this->product;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroup($group) {
        $this->group = $group;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setCantFreeExtra($cantFreeExtra) {
        $this->cantFreeExtra = $cantFreeExtra;
    }

    function setRequired($required) {
        $this->required = $required;
    }

    function setOrder($order) {
        $this->order = $order;
    }

    function setMerchant($merchant) {
        $this->merchant = $merchant;
    }

    function setProduct($product) {
        $this->product = $product;
    }


}

