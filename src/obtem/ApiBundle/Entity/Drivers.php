<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Drivers
 *
 * @ORM\Table(name="drivers", uniqueConstraints={@ORM\UniqueConstraint(name="code_driver", columns={"code"})})
 * @ORM\Entity
 */
class Drivers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=250, nullable=true)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=200, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone_house", type="string", length=200, nullable=true)
     */
    private $telephoneHouse;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=200, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="profile_image", type="string", length=250, nullable=true)
     */
    private $profileImage;

    /**
     * @var string
     *
     * @ORM\Column(name="show_name", type="string", length=250, nullable=true)
     */
    private $showName;

    /**
     * @var integer
     *
     * @ORM\Column(name="receive_notifications", type="integer", nullable=true)
     */
    private $receiveNotifications;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=255, nullable=true)
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="device_type", type="string", length=10, nullable=true)
     */
    private $deviceType;

    /**
     * @var integer
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="date", nullable=true)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="driver_license", type="string", length=45, nullable=true)
     */
    private $driverLicense;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=150, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_id", type="string", length=150, nullable=true)
     */
    private $stripeId;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_bank_id", type="string", length=150, nullable=true)
     */
    private $stripeBankId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="application_date", type="datetime", nullable=true)
     */
    private $applicationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="acceptance_date", type="datetime", nullable=true)
     */
    private $acceptanceDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_date", type="datetime", nullable=false)
     */
    private $updatedDate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=150, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=200, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=255, nullable=true)
     */
    private $bankName;

    /**
     * @var string
     *
     * @ORM\Column(name="pex_card", type="string", length=255, nullable=true)
     */
    private $pexCard;

    /**
     * @var string
     *
     * @ORM\Column(name="car", type="string", length=255, nullable=true)
     */
    private $car;

    /**
     * @var string
     *
     * @ORM\Column(name="cell", type="string", length=255, nullable=true)
     */
    private $cell;

    /**
     * @var string
     *
     * @ORM\Column(name="account_id", type="string", length=10, nullable=true)
     */
    private $accountId;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    function getId() {
        return $this->id;
    }

    function getFullName() {
        return $this->fullName;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return $this->password;
    }

    function getTelephoneHouse() {
        return $this->telephoneHouse;
    }

    function getPhone() {
        return $this->phone;
    }

    function getProfileImage() {
        return $this->profileImage;
    }

    function getShowName() {
        return $this->showName;
    }

    function getReceiveNotifications() {
        return $this->receiveNotifications;
    }

    function getDeviceId() {
        return $this->deviceId;
    }

    function getDeviceType() {
        return $this->deviceType;
    }

    function getActive() {
        return $this->active;
    }

    function getBirthdate() {
        return $this->birthdate;
    }

    function getGender() {
        return $this->gender;
    }

    function getDriverLicense() {
        return $this->driverLicense;
    }

    function getToken() {
        return $this->token;
    }

    function getStripeId() {
        return $this->stripeId;
    }

    function getStripeBankId() {
        return $this->stripeBankId;
    }

    function getStatus() {
        return $this->status;
    }

    function getCreatedDate() {
        return $this->createdDate;
    }

    function getApplicationDate() {
        return $this->applicationDate;
    }

    function getAcceptanceDate() {
        return $this->acceptanceDate;
    }

    function getUpdatedDate() {
        return $this->updatedDate;
    }

    function getCode() {
        return $this->code;
    }

    function getCity() {
        return $this->city;
    }

    function getBankName() {
        return $this->bankName;
    }

    function getPexCard() {
        return $this->pexCard;
    }

    function getCar() {
        return $this->car;
    }

    function getCell() {
        return $this->cell;
    }

    function getAccountId() {
        return $this->accountId;
    }

    function getComment() {
        return $this->comment;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setTelephoneHouse($telephoneHouse) {
        $this->telephoneHouse = $telephoneHouse;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setProfileImage($profileImage) {
        $this->profileImage = $profileImage;
    }

    function setShowName($showName) {
        $this->showName = $showName;
    }

    function setReceiveNotifications($receiveNotifications) {
        $this->receiveNotifications = $receiveNotifications;
    }

    function setDeviceId($deviceId) {
        $this->deviceId = $deviceId;
    }

    function setDeviceType($deviceType) {
        $this->deviceType = $deviceType;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setBirthdate(\DateTime $birthdate) {
        $this->birthdate = $birthdate;
    }

    function setGender($gender) {
        $this->gender = $gender;
    }

    function setDriverLicense($driverLicense) {
        $this->driverLicense = $driverLicense;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function setStripeId($stripeId) {
        $this->stripeId = $stripeId;
    }

    function setStripeBankId($stripeBankId) {
        $this->stripeBankId = $stripeBankId;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setCreatedDate( $createdDate) {
        $this->createdDate = $createdDate;
    }

    function setApplicationDate( $applicationDate) {
        $this->applicationDate = $applicationDate;
    }

    function setAcceptanceDate( $acceptanceDate) {
        $this->acceptanceDate = $acceptanceDate;
    }

    function setUpdatedDate( $updatedDate) {
        $this->updatedDate = $updatedDate;
    }

    function setCode($code) {
        $this->code = $code;
    }

    function setCity($city) {
        $this->city = $city;
    }

    function setBankName($bankName) {
        $this->bankName = $bankName;
    }

    function setPexCard($pexCard) {
        $this->pexCard = $pexCard;
    }

    function setCar($car) {
        $this->car = $car;
    }

    function setCell($cell) {
        $this->cell = $cell;
    }

    function setAccountId($accountId) {
        $this->accountId = $accountId;
    }

    function setComment($comment) {
        $this->comment = $comment;
    }


}

