<?php

namespace obtem\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice", uniqueConstraints={@ORM\UniqueConstraint(name="id_2", columns={"id"})}, indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="id_3", columns={"id"}), @ORM\Index(name="merchant_id", columns={"merchant_id"})})
 * @ORM\Entity(repositoryClass="obtem\ApiBundle\Entity\GeneralRepository")
 */
class Invoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="merchant_id", type="integer", nullable=false)
     */
    private $merchantId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=200, nullable=true)
     */
    private $userName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="issued_date", type="datetime", nullable=true)
     */
    private $issuedDate;
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     */
    private $startDate = 'CURRENT_TIMESTAMP';
    
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate = 'CURRENT_TIMESTAMP';
    
    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", precision=10, scale=0, nullable=false)
     */
    private $total;
    
    
    function getId() {
        return $this->id;
    }

    function getMerchantId() {
        return $this->merchantId;
    }
    function getTotal() {
        return $this->total;
    }

    function setTotal($total) {
        $this->total = $total;
    }

        function getUserName() {
        return $this->userName;
    }

    function getIssuedDate() {
        return $this->issuedDate;
    }

    function getStartDate() {
        return $this->startDate;
    }

    function getEndDate() {
        return $this->endDate;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setMerchantId($merchantId) {
        $this->merchantId = $merchantId;
    }

    function setUserName($userName) {
        $this->userName = $userName;
    }

    function setIssuedDate(\DateTime $issuedDate) {
        $this->issuedDate = $issuedDate;
    }

    function setStartDate(\DateTime $startDate) {
        $this->startDate = $startDate;
    }

    function setEndDate(\DateTime $endDate) {
        $this->endDate = $endDate;
    }



}

